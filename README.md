# Amaury VANOORENBERGHE - CV Web
[Consulter mon CV](http://amaury-vanoorenberghe.gitlab.io)

## Roadmap
- [x] utiliser un template pour avoir une première version en ligne rapidement
- [ ] ajouter la liste de mes projets personnels
- [ ] réaliser une page CV web de zéro (sans template)
- [ ] rendre ma page personnalisée responsive pour l'accessibilité sur les terminaux mobiles

## License et attributions
Ce CV a été réalisé à l'aide du template `Clarence TAYLOR` disponible à cette adresse: https://startbootstrap.com/previews/resume